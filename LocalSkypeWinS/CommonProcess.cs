﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocalSkypeWinS
{
    public static class CommonProcess
    {
        public static MbsContactCall CreateMbsContactCall(SkypeLocal dump, MbsContactPhone mbsPhone, string phoneNo)
        {

            var contactEmail = new MbsContactCall()
            {
                CallDate = dump.CallDate,
                Amount = decimal.Parse(dump.Amount),
                Rate = decimal.Parse(dump.Rate),
                CallLogSource = "Skype",
                CallLogType = "Incoming Call",
                FromContactId = dump.CallerContactId,
                FromPhoneNumber = phoneNo,
                ToPhoneNumber = dump.PhoneNumber,
                CreatedOn = DateTime.Now,
                CallDuration = dump.Duration


            };
            return contactEmail;
        }

        public static MbsContactCall AddMbsContactCall(MbsContactCall mbsContactCall)
        {
            try
            {
                using (var context = new mbssourceEntities())
                {
                    if (context.MbsContactCalls.FirstOrDefault(
                            x => x.CallDate == mbsContactCall.CallDate && x.ToPhoneNumber == mbsContactCall.ToPhoneNumber && x.FromPhoneNumber == mbsContactCall.FromPhoneNumber && x.CallDuration == mbsContactCall.CallDuration) == null)
                    {
                        var addedmbsContactEmail = context.MbsContactCalls.Add(mbsContactCall);
                        context.SaveChanges();
                        return addedmbsContactEmail;
                    }
                    return null;
                }
            }
            catch (Exception ex)
            {
                Trace.TraceInformation("Could not save email to db 1.9" + ex.Message);
                return null;
            }
        }

        public static MbsContactPhone PhoneExistsInMbs(string phone)
        {

            try
            {
                using (var context = new mbssourceEntities())
                {
                    var returnEmail = context.MbsContactPhones.FirstOrDefault(x => x.Phone == phone || x.Phone.Contains(phone));
                    return returnEmail ?? null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static SkypeLocal AddSkypeDump(SkypeLocal dump)
        {

            try
            {
                using (var context = new EmailDumpEntities())
                {
                    if (context.SkypeLocals.FirstOrDefault(x => x.CallDate == dump.CallDate && x.CallerContactId == dump.CallerContactId) == null)
                    {
                        var addeDump = context.SkypeLocals.Add(dump);
                        context.SaveChanges();
                        return addeDump;
                    }
                    return null;
                }
            }
            catch (Exception ex)
            {
                Trace.TraceInformation("Could not save mail to db0" + ex.Message);
                return null;
            }
        }

        public static MbsContactActivity CreateActivity(SkypeLocal dump, MbsContactCall mbsContactCall, MbsContactPhone phone)
        {

            var contactActivity = new MbsContactActivity()
            {
                CreatedOn = DateTime.UtcNow,
                ActivityType = 4,
                ReferenceId = mbsContactCall.CallId,
                ContactId = phone.ContactId,
                Description = "Copied from Skype Call dump"
            };
            return contactActivity;
        }

        public static MbsContactActivity AddActivity(MbsContactActivity activity)
        {
            try
            {
                using (var context = new mbssourceEntities())
                {
                    var addedActivity = context.MbsContactActivities.Add(activity);
                    context.SaveChanges();
                    return addedActivity;
                }
            }
            catch (Exception ex)
            {
                Trace.TraceInformation("Could not save activity to db0" + ex.Message);
                return null;
            }
        }
    }
}
