﻿using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace LocalSkypeWinS
{
    public partial class Service1 : ServiceBase
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        Timer _timer;
        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            //log.Info("Info - Service Started");
            var checkInterval = Convert.ToInt32(ConfigurationManager.AppSettings["ProcessInterval"]);
            _timer = new Timer(checkInterval * 60 * 1000); // every 10 minutes??
            _timer.Elapsed += new System.Timers.ElapsedEventHandler(SkypeCallsProcess);
            _timer.Start();
        }

        private void SkypeCallsProcess(object sender, System.Timers.ElapsedEventArgs e)
        {
            //log.Info("Info - Check time");
            DateTime startAt = DateTime.Today.AddHours(9).AddMinutes(48);
            if (_lastRun < startAt && DateTime.Now >= startAt)
            {
                // stop the timer 
                _timer.Stop();

                try
                {
                    log.Info("Info - Import");
                    SmartImportService.WebService.WebServiceSoapClient test = new WebService.WebServiceSoapClient();
                    test.Import();
                }
                catch (Exception ex)
                {
                    log.Error("This is my error - ", ex);
                }

                _lastRun = DateTime.Now;
                _timer.Start();
            }
        }

        protected override void OnStop()
        {
        }
    }
}
