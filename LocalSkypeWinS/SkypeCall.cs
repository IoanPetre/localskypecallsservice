﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadSkypeDB
{
    public class SkypeCall
    {
        public DateTime CallDate { get; set; }
        public string PhoneNumber { get; set; }
        public string Destination { get; set; }
        public string Type { get; set; }
        public string Rate { get; set; }
        public string Duration { get; set; }
        public string Amount { get; set; }
        public string Currency { get; set; }
        public Guid CallerContactId { get; set; }
        public Guid ReceiverContactId { get; set; }
        public string CallerPhoneNumber { get; set; }
        public char IsUnmached { get; set; }
    }
}