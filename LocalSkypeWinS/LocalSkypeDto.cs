﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocalSkypeWinS
{
    public class LocalSkypeDto
    {
        public string PhoneNumber { get; set; }
        public int Type { get; set; }
        public int? Duration { get; set; }
        public string Currency { get; set; }
        public long CallDate { get; set; }
        public long StartTime { get; set; }
        public string ForwardedBy { get; set; }
        public int? PricePrecision { get; set; }
        public int Rate { get; set; }
        public string Username { get; set; }
    }
}
