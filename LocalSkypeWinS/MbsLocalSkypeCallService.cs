﻿using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SQLite;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using MbsSkypeCallsLibrary;
using Newtonsoft.Json;

namespace LocalSkypeWinS
{
    public partial class MbsLocalSkypeCallService : ServiceBase
    {
        private static readonly Logger log = LogManager.GetCurrentClassLogger();
        Timer _timer;
        public MbsLocalSkypeCallService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            log.Info("Info - Service Started");
            var checkInterval = Convert.ToInt32(ConfigurationManager.AppSettings["ProcessInterval"]);
            _timer = new Timer(checkInterval * 60 * 1000);
            _timer.Elapsed += SkypeCallsProcess;
            _timer.Start();
        }

        public void SkypeCallsProcess(object sender, ElapsedEventArgs e)
        {
            log.Info("SkypeCallsProcess");
            _timer.Stop();
            var manager = new SkypeCallsProcess();
            try
            {
                var calls = manager.GetLocalSkypeCalls();
                if (calls == null)
                {
                    log.Info("No SkypeCalls to Process");
                    return;
                }
                manager.ProcessSkypeCalls(calls);
            }
            catch (Exception ex)
            {
                log.Error("Exception - ", ex);
            }

            _timer.Start();
        }

        protected override void OnStop()
        {
        }
    }
}
