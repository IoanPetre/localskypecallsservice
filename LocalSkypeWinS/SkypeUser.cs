﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocalSkypeWinS
{
    class SkypeUser
    {
        public string Username { get; set; }
        public string Pw { get; set; }
        public string MbsContactId { get; set; }
    }
}
