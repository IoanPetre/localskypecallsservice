﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace MbsSkypeCallsLibrary
{
    public static class Helpers
    {
        public static List<T> ConvertDataTable<T>(DataTable dt)
        {
            List<T> data = new List<T>();
            foreach (DataRow row in dt.Rows)
            {
                T item = GetItem<T>(row);
                data.Add(item);
            }
            return data;
        }

        public static DateTime UnixTimestampToDatetime(this long timestamp)
        {
            return new DateTime(1970, 1, 1).AddSeconds(timestamp);
        }

        private static T GetItem<T>(DataRow dr)
        {
            Type temp = typeof(T);
            T obj = Activator.CreateInstance<T>();

            foreach (DataColumn column in dr.Table.Columns)
            {
                foreach (PropertyInfo pro in temp.GetProperties())
                {
                    if (pro.Name == column.ColumnName && dr[column.ColumnName]!= null)
                    {
                        var collValue = dr[column.ColumnName];
                        if (collValue != System.DBNull.Value)
                        {
                            pro.SetValue(obj, collValue, null);
                        }
                        
                    }
                        
                    else
                        continue;
                }
            }
            return obj;
        }
    }
}
