﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MbsSkypeCallsLibrary
{
    public class LocalSkypeDto
    {
        public string PhoneNumber { get; set; }
        public long Type { get; set; }
        public long? Duration { get; set; }
        public string Currency { get; set; }
        public string Username { get; set; }
        public long CallDate { get; set; }
        public long StartTime { get; set; }
        public string ForwardedBy { get; set; }
        public long? PricePrecision { get; set; }
        public long Rate { get; set; }
    }
}
