﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MbsSkypeCallsLibrary
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class EmailDumpEntities : DbContext
    {
        public EmailDumpEntities()
            : base("name=EmailDumpEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<AzureADUserEmail> AzureADUserEmails { get; set; }
        public virtual DbSet<BlacklistEmail> BlacklistEmails { get; set; }
        public virtual DbSet<SkypeDump> SkypeDumps { get; set; }
        public virtual DbSet<SkypeLocal> SkypeLocals { get; set; }
    }
}
