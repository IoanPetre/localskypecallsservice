﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SQLite;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using NLog;

namespace MbsSkypeCallsLibrary
{
    public class SkypeCallsProcess
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        private List<string> _blacklistEmails;

        public void ProcessSkypeCalls(List<SkypeLocal> calls)
        {
            foreach (var skypeDump in calls)
            {
                var mbsPhone = PhoneExistsInMbs(skypeDump.PhoneNumber);
                if (mbsPhone != null)
                {
                    //                        var mbsCall = CommonProcess.CreateMbsContactCall(skypeDump, mbsPhone, skypeDump.CallerPhoneNumber);
                    //                        var addedMbsCall = CommonProcess.AddMbsContactCall(mbsCall);
                    //                        if (addedMbsCall != null)
                    //                        {
                    //                            var activity = CommonProcess.CreateActivity(skypeDump, addedMbsCall, mbsPhone);
                    //                            var addedActivity = CommonProcess.AddActivity(activity);
                    //                        }

                    skypeDump.ReceiverContactId = mbsPhone.ContactId;
                    skypeDump.IsUnmached = false;
                    var addedDump = AddSkypeDump(skypeDump);
                }
                else
                {
                    var addedDump = AddSkypeDump(skypeDump);
                }
            }
        }

        public List<SkypeLocal> GetLocalSkypeCalls()
        {
            var calls = new List<SkypeLocal>();
            var firstDbRow = ReadFromLocalDb(@"select cm.[identity] as PhoneNumber, cm.[type] Type, cm.[call_duration] Duration, cm.[price_currency] as Currency,
            c.host_identity as Username, c.begin_timestamp as CallDate, cm.start_timestamp StartTime, cm.forwarded_by ForwardedBy,
                cm.price_precision PricePrecision, cm.price_per_minute Rate
            from CallMembers cm
                inner
            join Calls c on cm.call_name = c.name
            where cm.[type] IN(3, 4) and host_identity IS NOT NULL LIMIT 1");
            if (firstDbRow.Count == 0)
            {
                return null;
            }
            List<SkypeUser> users = JsonConvert.DeserializeObject<List<SkypeUser>>(ConfigurationManager.AppSettings["UserLogins"]);
            var currentUserName = firstDbRow.First().Username;
            var currentUserMbsContactId = users.FirstOrDefault(s => s.Username == currentUserName)?.MbsContactId;
            if (string.IsNullOrEmpty(currentUserMbsContactId))
            {
                Log.Info("No user found");
                return null;
            }
            var currentUserPhone = GetUsersPhones()
                .FirstOrDefault(s => s.MbsContactId == new Guid(currentUserMbsContactId))?.PhoneNo;

            using (var context = new EmailDumpEntities())
            {
                var latestSkypeDumpRecord = context.SkypeDumps
                    .Where(g => g.CallerContactId.ToString() == currentUserMbsContactId && g.CallDate.HasValue)
                    .OrderByDescending(g => g.CallDate).FirstOrDefault();
                var latestDateInUnixTime = latestSkypeDumpRecord?.CallDate != null
                    ? ((DateTimeOffset) latestSkypeDumpRecord.CallDate).ToUnixTimeSeconds()
                    : 0;
                var localCalls =
                    ReadFromLocalDb(
                        $@"select cm.[identity] as PhoneNumber, cm.[type] Type, cm.[call_duration] Duration, cm.[price_currency] as Currency,
                    c.host_identity as Username, c.begin_timestamp as CallDate, cm.start_timestamp StartTime, cm.forwarded_by ForwardedBy,
                    cm.price_precision PricePrecision, cm.price_per_minute Rate 
                    from CallMembers cm 
                    inner join Calls c on cm.call_name = c.name 
                    where cm.[type] IN(3,4) and identity_type = 4  and is_incoming = 0 and CallDate > {
                                latestDateInUnixTime
                            }");

                foreach (var localdbCall in localCalls)
                {
                    var call = new SkypeLocal();
                    call.CallDate = localdbCall.CallDate.UnixTimestampToDatetime();
                    var destination = ExtractDestinationFromPhoneNumber(localdbCall.PhoneNumber);
                    call.PhoneNumber = localdbCall.PhoneNumber.Remove(0, 2); //todo: extract int calls prefix
                    call.Destination = destination;
                    call.Rate = (localdbCall.Rate / 1000).ToString();

                    TimeSpan durationTimespan = TimeSpan.FromSeconds(localdbCall.Duration ?? 0);
                    string duration =
                        $"{durationTimespan.Hours:D2}:{durationTimespan.Minutes:D2}:{durationTimespan.Seconds:D2}";
                    call.Duration = duration;
                    call.Type = ExtractCallTypeFromlocalDbType(localdbCall.Type);
                    call.Amount = "0";
                    call.Currency = localdbCall.Currency ?? "USD";
                    call.CallerContactId = new Guid(currentUserMbsContactId);
                    call.CallerPhoneNumber = currentUserPhone;
                    call.IsUnmached = !IsPhoneBlacliksted(call.PhoneNumber);
                    calls.Add(call);
                }
            }
            return calls;
        }

        private MbsContactCall CreateMbsContactCall(SkypeDump dump, MbsContactPhone mbsPhone, string phoneNo)
        {
            var contactEmail = new MbsContactCall()
            {
                CallDate = dump.CallDate,
                Amount = decimal.Parse(dump.Amount),
                Rate = decimal.Parse(dump.Rate),
                CallLogSource = "Skype",
                CallLogType = "Incoming Call",
                FromContactId = dump.CallerContactId,
                FromPhoneNumber = phoneNo,
                ToPhoneNumber = dump.PhoneNumber,
                CreatedOn = DateTime.Now,
                CallDuration = dump.Duration
            };
            return contactEmail;
        }

        private MbsContactCall AddMbsContactCall(MbsContactCall mbsContactCall)
        {
            try
            {
                using (var context = new mbssourceEntities())
                {
                    if (context.MbsContactCalls.FirstOrDefault(
                            x => x.CallDate == mbsContactCall.CallDate && x.ToPhoneNumber == mbsContactCall.ToPhoneNumber && x.FromPhoneNumber == mbsContactCall.FromPhoneNumber && x.CallDuration == mbsContactCall.CallDuration) == null)
                    {
                        var addedmbsContactEmail = context.MbsContactCalls.Add(mbsContactCall);
                        context.SaveChanges();
                        return addedmbsContactEmail;
                    }
                    return null;
                }
            }
            catch (Exception ex)
            {
                Trace.TraceInformation("Could not save email to db 1.9" + ex.Message);
                return null;
            }
        }

        private MbsContactPhone PhoneExistsInMbs(string phone)
        {

            try
            {
                using (var context = new mbssourceEntities())
                {
                    var returnEmail = context.MbsContactPhones.FirstOrDefault(x => x.Phone == phone || x.Phone.Contains(phone));
                    return returnEmail ?? null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private SkypeLocal AddSkypeDump(SkypeLocal dump)
        {

            try
            {
                using (var context = new EmailDumpEntities())
                {
                    if (context.SkypeLocals.FirstOrDefault(x => x.CallDate == dump.CallDate && x.CallerContactId == dump.CallerContactId) == null)
                    {
                        var addeDump = context.SkypeLocals.Add(dump);
                        context.SaveChanges();
                        return addeDump;
                    }
                    return null;
                }
            }
            catch (Exception ex)
            {
                Trace.TraceInformation("Could not save mail to db0" + ex.Message);
                return null;
            }
        }

        private MbsContactActivity CreateActivity(SkypeDump dump, MbsContactCall mbsContactCall, MbsContactPhone phone)
        {

            var contactActivity = new MbsContactActivity()
            {
                CreatedOn = DateTime.UtcNow,
                ActivityType = 4,
                ReferenceId = mbsContactCall.CallId,
                ContactId = phone.ContactId,
                Description = "Copied from Skype Call dump"
            };
            return contactActivity;
        }

        private MbsContactActivity AddActivity(MbsContactActivity activity)
        {
            try
            {
                using (var context = new mbssourceEntities())
                {
                    var addedActivity = context.MbsContactActivities.Add(activity);
                    context.SaveChanges();
                    return addedActivity;
                }
            }
            catch (Exception ex)
            {
                Trace.TraceInformation("Could not save activity to db0" + ex.Message);
                return null;
            }
        }

        private string ExtractCallTypeFromlocalDbType(long localdbCallType)
        {
            var result = "";
            switch (localdbCallType)
            {
                case 4:
                    result = "Call";
                    break;
                case 3:
                    result = "Call Forward";
                    break;
                default:
                    result = "";
                    break;
            }
            return result;
        }

        private static List<AzureADUserEmail> GetUsersPhones()
        {
            try
            {
                using (var context = new EmailDumpEntities())
                {

                    var lSubscriptions = context.AzureADUserEmails.Where(x => x.PhoneNo != null).ToList();
                    return lSubscriptions ?? null;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.InnerException.Message);
                return null;
            }
        }

        private bool IsPhoneBlacliksted(string phone)
        {
            try
            {

                if (_blacklistEmails == null)
                {
                    using (var context = new EmailDumpEntities())
                    {
                        _blacklistEmails = context.BlacklistEmails.Select(x => x.Blacklisted).ToList();
                    }
                }
                return _blacklistEmails.Contains(phone);

            }
            catch (Exception ex)
            {
                Trace.TraceInformation("Error on countig blacklisted" + ex.InnerException.Message);
                return false;
            }
        }

        private string ExtractDestinationFromPhoneNumber(string phoneNumber)
        {
            if (phoneNumber.Contains("+1"))
            {
                return "United States";
            }
            else
            {
                return "Other";
            }
        }

        private List<LocalSkypeDto> ReadFromLocalDb(string sqlCommand)
        {
            var result = new List<LocalSkypeDto>();
            using (SQLiteConnection con = new SQLiteConnection("data source = " + ConfigurationManager.AppSettings["skypeDbPath"]))
            {
                using (SQLiteCommand com = new SQLiteCommand(con))
                {
                    con.Open();
                    com.CommandText = sqlCommand;
                    using (SQLiteDataReader reader = com.ExecuteReader())
                    {
                        DataTable dt = new DataTable();
                        dt.Load(reader);
                        result = Helpers.ConvertDataTable<LocalSkypeDto>(dt);
                    }
                    con.Close();
                }
            }
            return result;
        }

    }
}
