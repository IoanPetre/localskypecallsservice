﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MbsSkypeCallsLibrary;

namespace LocalSkypeConsoleApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            var manager = new SkypeCallsProcess();
            try
            {
                var calls = manager.GetLocalSkypeCalls();
                if (calls == null)
                {
                    Console.WriteLine("No SkypeCalls to Process");
                    return;
                }
                manager.ProcessSkypeCalls(calls);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception - ", ex);
            }
        }
    }
}
